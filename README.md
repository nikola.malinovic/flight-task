# Flight Task Documentation

## Requirements

- Java 17
- Docker

## How to run Locally?

1. Run database from the root directory of repository:

```
docker-compose -f flight-task-db.yml up
```

2. Add environment variables:

```
FLIGHT_DB_USERNAME=postgres
FLIGHT_DB_PASSWORD=postgres
```

3. Run [FlightTaskApplication](src/main/java/org/task/flighttask/FlightTaskApplication.java).

## Database connection details

| Option   | Value                                     |
|----------|:------------------------------------------|
| port     | 5432                                      |
| username | postgres                                  |
| password | postgres                                  |
| Database | postgres                                  |
| URL      | jdbc:postgresql://localhost:5432/postgres |

## How to use Swagger?

1. Start Flight Task application
2. Enter following URL in browser:

```
http://localhost:8080/swagger-ui.html
```

## How to insert dummy data?

1. Start Flight Task application

2. Generate Flight objects using [JSON Generator](https://json-generator.com/), with following
   template:

```
[
 '{{repeat(5)}}',
 {
 flightId: '{{index()}}',
 flightNumber: '{{integer(1000, 9999)}}',
 departureAirportIATACode: '{{random("SEA","YYZ","YYT","ANC","LAX")}}',
 arrivalAirportIATACode: '{{random("MIT","LEW","GDN","KRK","PPX")}}',
 departureDate: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss Z")}}'
 }
]
```

2. Make [Add Flights](#add-flights) HTTP Request with generated value as a Request Body.

3. Generate Cargo objects using [JSON Generator](https://json-generator.com/), with following
   template:

```
[
 '{{repeat(5)}}',
 {
 flightId: '{{index()}}',
 baggage: [
 '{{repeat(3,8)}}',
 {
 id: '{{index()}}',
 weight: '{{integer(1, 999)}}',
 weightUnit: '{{random("kg","lb")}}',
 pieces: '{{integer(1, 999)}}'
 }
 ],
 cargo: [
 '{{repeat(3,5)}}',
 {
 id: '{{index()}}',
 weight: '{{integer(1, 999)}}',
 weightUnit: '{{random("kg","lb")}}',
 pieces: '{{integer(1, 999)}}'
 }
 ]
 }
]
```

4. Make [Add Cargo](#add-cargo) HTTP Request with generated value as a Request Body.

## Endpoints

### Add Flights

POST /flights

Description: Stores provided Flight objects into database.

Request Body example:

```json
[
  {
    "flightId": 0,
    "flightNumber": 9193,
    "departureAirportIATACode": "LAX",
    "arrivalAirportIATACode": "MIT",
    "departureDate": "2016-05-22T06:51:38 -02:00"
  },
  {
    "flightId": 1,
    "flightNumber": 5924,
    "departureAirportIATACode": "LAX",
    "arrivalAirportIATACode": "LEW",
    "departureDate": "2020-10-02T08:54:19 -02:00"
  },
  {
    "flightId": 2,
    "flightNumber": 2102,
    "departureAirportIATACode": "ANC",
    "arrivalAirportIATACode": "PPX",
    "departureDate": "2021-06-16T10:31:02 -02:00"
  },
  {
    "flightId": 3,
    "flightNumber": 1869,
    "departureAirportIATACode": "YYZ",
    "arrivalAirportIATACode": "PPX",
    "departureDate": "2015-06-10T06:58:34 -02:00"
  },
  {
    "flightId": 4,
    "flightNumber": 2229,
    "departureAirportIATACode": "LAX",
    "arrivalAirportIATACode": "GDN",
    "departureDate": "2022-08-27T07:59:57 -02:00"
  }
]
```

### Add Cargo

POST /cargo

Description: Stores provided Cargo objects into database.

Request Body example:

```json
[
  {
    "flightId": 0,
    "baggage": [
      {
        "id": 0,
        "weight": 475,
        "weightUnit": "kg",
        "pieces": 280
      },
      {
        "id": 1,
        "weight": 818,
        "weightUnit": "lb",
        "pieces": 279
      },
      {
        "id": 2,
        "weight": 175,
        "weightUnit": "lb",
        "pieces": 459
      },
      {
        "id": 3,
        "weight": 129,
        "weightUnit": "lb",
        "pieces": 702
      },
      {
        "id": 4,
        "weight": 435,
        "weightUnit": "lb",
        "pieces": 419
      },
      {
        "id": 5,
        "weight": 115,
        "weightUnit": "lb",
        "pieces": 744
      },
      {
        "id": 6,
        "weight": 464,
        "weightUnit": "lb",
        "pieces": 466
      },
      {
        "id": 7,
        "weight": 10,
        "weightUnit": "kg",
        "pieces": 868
      }
    ],
    "cargo": [
      {
        "id": 0,
        "weight": 156,
        "weightUnit": "kg",
        "pieces": 609
      },
      {
        "id": 1,
        "weight": 121,
        "weightUnit": "lb",
        "pieces": 489
      },
      {
        "id": 2,
        "weight": 665,
        "weightUnit": "kg",
        "pieces": 68
      },
      {
        "id": 3,
        "weight": 981,
        "weightUnit": "kg",
        "pieces": 81
      },
      {
        "id": 4,
        "weight": 659,
        "weightUnit": "kg",
        "pieces": 507
      }
    ]
  },
  {
    "flightId": 1,
    "baggage": [
      {
        "id": 0,
        "weight": 52,
        "weightUnit": "kg",
        "pieces": 544
      },
      {
        "id": 1,
        "weight": 870,
        "weightUnit": "lb",
        "pieces": 917
      },
      {
        "id": 2,
        "weight": 143,
        "weightUnit": "lb",
        "pieces": 528
      },
      {
        "id": 3,
        "weight": 222,
        "weightUnit": "kg",
        "pieces": 39
      },
      {
        "id": 4,
        "weight": 193,
        "weightUnit": "lb",
        "pieces": 171
      },
      {
        "id": 5,
        "weight": 817,
        "weightUnit": "lb",
        "pieces": 176
      }
    ],
    "cargo": [
      {
        "id": 0,
        "weight": 917,
        "weightUnit": "kg",
        "pieces": 641
      },
      {
        "id": 1,
        "weight": 952,
        "weightUnit": "lb",
        "pieces": 401
      },
      {
        "id": 2,
        "weight": 945,
        "weightUnit": "kg",
        "pieces": 265
      },
      {
        "id": 3,
        "weight": 580,
        "weightUnit": "kg",
        "pieces": 327
      },
      {
        "id": 4,
        "weight": 582,
        "weightUnit": "kg",
        "pieces": 921
      }
    ]
  },
  {
    "flightId": 2,
    "baggage": [
      {
        "id": 0,
        "weight": 267,
        "weightUnit": "kg",
        "pieces": 890
      },
      {
        "id": 1,
        "weight": 738,
        "weightUnit": "kg",
        "pieces": 450
      },
      {
        "id": 2,
        "weight": 661,
        "weightUnit": "kg",
        "pieces": 206
      },
      {
        "id": 3,
        "weight": 565,
        "weightUnit": "kg",
        "pieces": 189
      },
      {
        "id": 4,
        "weight": 596,
        "weightUnit": "lb",
        "pieces": 485
      },
      {
        "id": 5,
        "weight": 266,
        "weightUnit": "lb",
        "pieces": 75
      },
      {
        "id": 6,
        "weight": 798,
        "weightUnit": "lb",
        "pieces": 423
      },
      {
        "id": 7,
        "weight": 581,
        "weightUnit": "lb",
        "pieces": 794
      }
    ],
    "cargo": [
      {
        "id": 0,
        "weight": 745,
        "weightUnit": "kg",
        "pieces": 212
      },
      {
        "id": 1,
        "weight": 170,
        "weightUnit": "kg",
        "pieces": 475
      },
      {
        "id": 2,
        "weight": 331,
        "weightUnit": "lb",
        "pieces": 269
      },
      {
        "id": 3,
        "weight": 323,
        "weightUnit": "lb",
        "pieces": 85
      },
      {
        "id": 4,
        "weight": 633,
        "weightUnit": "kg",
        "pieces": 141
      }
    ]
  },
  {
    "flightId": 3,
    "baggage": [
      {
        "id": 0,
        "weight": 674,
        "weightUnit": "lb",
        "pieces": 202
      },
      {
        "id": 1,
        "weight": 723,
        "weightUnit": "lb",
        "pieces": 944
      },
      {
        "id": 2,
        "weight": 159,
        "weightUnit": "lb",
        "pieces": 632
      },
      {
        "id": 3,
        "weight": 664,
        "weightUnit": "kg",
        "pieces": 204
      },
      {
        "id": 4,
        "weight": 445,
        "weightUnit": "kg",
        "pieces": 633
      },
      {
        "id": 5,
        "weight": 513,
        "weightUnit": "kg",
        "pieces": 839
      },
      {
        "id": 6,
        "weight": 212,
        "weightUnit": "kg",
        "pieces": 492
      },
      {
        "id": 7,
        "weight": 615,
        "weightUnit": "kg",
        "pieces": 438
      }
    ],
    "cargo": [
      {
        "id": 0,
        "weight": 977,
        "weightUnit": "kg",
        "pieces": 88
      },
      {
        "id": 1,
        "weight": 995,
        "weightUnit": "lb",
        "pieces": 738
      },
      {
        "id": 2,
        "weight": 761,
        "weightUnit": "lb",
        "pieces": 424
      },
      {
        "id": 3,
        "weight": 395,
        "weightUnit": "lb",
        "pieces": 209
      },
      {
        "id": 4,
        "weight": 670,
        "weightUnit": "kg",
        "pieces": 271
      }
    ]
  },
  {
    "flightId": 4,
    "baggage": [
      {
        "id": 0,
        "weight": 157,
        "weightUnit": "lb",
        "pieces": 972
      },
      {
        "id": 1,
        "weight": 300,
        "weightUnit": "kg",
        "pieces": 633
      },
      {
        "id": 2,
        "weight": 387,
        "weightUnit": "kg",
        "pieces": 339
      },
      {
        "id": 3,
        "weight": 775,
        "weightUnit": "lb",
        "pieces": 313
      },
      {
        "id": 4,
        "weight": 261,
        "weightUnit": "lb",
        "pieces": 625
      },
      {
        "id": 5,
        "weight": 164,
        "weightUnit": "kg",
        "pieces": 859
      },
      {
        "id": 6,
        "weight": 92,
        "weightUnit": "lb",
        "pieces": 626
      },
      {
        "id": 7,
        "weight": 564,
        "weightUnit": "kg",
        "pieces": 746
      }
    ],
    "cargo": [
      {
        "id": 0,
        "weight": 151,
        "weightUnit": "kg",
        "pieces": 135
      },
      {
        "id": 1,
        "weight": 203,
        "weightUnit": "lb",
        "pieces": 779
      },
      {
        "id": 2,
        "weight": 34,
        "weightUnit": "lb",
        "pieces": 613
      },
      {
        "id": 3,
        "weight": 594,
        "weightUnit": "kg",
        "pieces": 799
      },
      {
        "id": 4,
        "weight": 164,
        "weightUnit": "lb",
        "pieces": 134
      }
    ]
  }
]
```

### Get Weight Statistics

GET /weight-statistics?flight-number=9193&date=2016-05-22

Query Parameters:

- flight-number
- date

Description: For requested Flight Number and Date, returns Flight statistics.

### Get Baggage Statistics

GET /baggage-statistics?iata-code=LAX&date=2016-05-22

Query Parameters:

- iata-code
- date

Description: For requested IATA Code and Date, returns Cargo statistics.
