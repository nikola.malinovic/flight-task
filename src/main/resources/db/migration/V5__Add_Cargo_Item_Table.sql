CREATE TABLE IF NOT EXISTS cargo_item
(
    id          int PRIMARY KEY,
    flight_id   int,
    weight      int,
    weight_unit varchar(2),
    pieces      int,
    cargo_type  varchar(7),
    CONSTRAINT fk_flight_id FOREIGN KEY (flight_id) REFERENCES flight (flight_id)
);

create index cargo_item_cargo_type_index on cargo_item (cargo_type);
