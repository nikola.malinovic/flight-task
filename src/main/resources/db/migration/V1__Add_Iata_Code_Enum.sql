CREATE TYPE iata_code AS ENUM (
    'SEA',
    'YYZ',
    'YYT',
    'ANC',
    'LAX',
    'LEW',
    'GDN',
    'KRK',
    'PPX'
    );
