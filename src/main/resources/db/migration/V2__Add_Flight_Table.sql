CREATE TABLE IF NOT EXISTS flight
(
    flight_id                   int PRIMARY KEY,
    flight_number               int,
    departure_airport_iata_code varchar(3),
    arrival_airport_iata_code   varchar(3),
    departure_date              timestamp
);

create index flight_flight_number_index on flight (flight_number);
