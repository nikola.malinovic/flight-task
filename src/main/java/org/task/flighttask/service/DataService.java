package org.task.flighttask.service;

import java.time.OffsetDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.task.flighttask.model.dto.CargoDto;
import org.task.flighttask.model.dto.CargoItemDto;
import org.task.flighttask.model.dto.FlightDto;
import org.task.flighttask.model.entity.CargoItem;
import org.task.flighttask.model.entity.CargoType;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.repository.CargoItemRepository;
import org.task.flighttask.repository.FlightRepository;

@Service
@AllArgsConstructor
public class DataService {

  private final FlightRepository flightRepository;
  private final CargoItemRepository cargoItemRepository;

  public List<Flight> addFlights(List<FlightDto> flights) {
    return flightRepository.saveAll(flights.stream().map(this::toFlight).toList());
  }

  public List<CargoDto> addCargo(List<CargoDto> cargos) {
    cargos.forEach(this::saveCargoItems);
    return cargos;
  }

  private Flight toFlight(FlightDto flightDto) {
    return Flight.builder()
        .flightId(flightDto.flightId())
        .flightNumber(flightDto.flightNumber())
        .departureAirportIATACode(flightDto.departureAirportIATACode())
        .arrivalAirportIATACode(flightDto.arrivalAirportIATACode())
        .departureDate(OffsetDateTime.parse(flightDto.departureDate().replaceAll("\\s", "")))
        .build();
  }

  private void saveCargoItems(CargoDto cargoDto) {
    cargoDto
        .baggage()
        .forEach(baggage -> saveCargoItems(baggage, cargoDto.flightId(), CargoType.BAGGAGE));
    
    cargoDto.cargo().forEach(cargo -> saveCargoItems(cargo, cargoDto.flightId(), CargoType.CARGO));
  }

  private void saveCargoItems(CargoItemDto cargo, int flightId, CargoType cargoType) {
    CargoItem cargoItem =
        CargoItem.builder()
            .flightId(flightId)
            .weight(cargo.weight())
            .weightUnit(cargo.weightUnit())
            .pieces(cargo.pieces())
            .cargoType(cargoType)
            .build();
    cargoItemRepository.save(cargoItem);
  }
}
