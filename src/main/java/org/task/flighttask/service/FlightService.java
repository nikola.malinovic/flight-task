package org.task.flighttask.service;

import static org.task.flighttask.util.WeightUtil.toKg;
import static org.task.flighttask.util.WeightUtil.toLb;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.task.flighttask.exception.FlightNotFoundException;
import org.task.flighttask.model.dto.BaggageStatisticsResponse;
import org.task.flighttask.model.dto.WeightStatisticsResponse;
import org.task.flighttask.model.entity.CargoItem;
import org.task.flighttask.model.entity.CargoType;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.model.entity.IataCode;
import org.task.flighttask.model.entity.WeightUnit;
import org.task.flighttask.repository.CargoItemRepository;
import org.task.flighttask.repository.FlightRepository;

@Service
@AllArgsConstructor
public class FlightService {

  private final FlightRepository flightRepository;
  private final CargoItemRepository cargoItemRepository;

  public WeightStatisticsResponse getWeightStatistics(int flightNumber, LocalDate date) {
    Flight flight = getFlight(flightNumber, date);

    List<CargoItem> cargo =
        cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.CARGO);
    List<CargoItem> baggage =
        cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.BAGGAGE);

    double cargoWeightKg = calculateCargoItemsWeightKg(cargo);
    double baggageWeightKg = calculateCargoItemsWeightKg(baggage);
    double totalWeightKg = cargoWeightKg + baggageWeightKg;

    return new WeightStatisticsResponse(
        cargoWeightKg,
        toLb(cargoWeightKg),
        baggageWeightKg,
        toLb(baggageWeightKg),
        totalWeightKg,
        toLb(totalWeightKg));
  }

  public BaggageStatisticsResponse getBaggageStatistics(IataCode iataCode, LocalDate date) {

    List<Flight> departureFlights = getDepartureFlights(iataCode, date);
    List<Flight> arrivalFlights = getArrivalFlights(iataCode, date);

    return new BaggageStatisticsResponse(
        departureFlights.size(),
        arrivalFlights.size(),
        getTotalPieces(arrivalFlights),
        getTotalPieces(departureFlights));
  }

  private Flight getFlight(int flightNumber, LocalDate date) {
    return flightRepository.findAllByFlightNumber(flightNumber).stream()
        .filter(flight -> isEqualDate.test(flight, date))
        .findAny()
        .orElseThrow(() -> new FlightNotFoundException("Flight does not exist"));
  }

  private double calculateCargoItemsWeightKg(List<CargoItem> cargoItems) {
    return cargoItems.stream()
        .map(this::getWeightKg)
        .reduce(Double::sum)
        .orElse(0.0);
  }

  private double getWeightKg(CargoItem cargoItem) {
    return cargoItem.getWeightUnit() == WeightUnit.kg
        ? cargoItem.getWeight()
        : toKg(cargoItem.getWeight());
  }

  private Integer getTotalPieces(List<Flight> flights) {
    return flights.stream()
        .map(Flight::getFlightId)
        .map(id -> cargoItemRepository.findByFlightIdAndCargoType(id, CargoType.BAGGAGE))
        .flatMap(Collection::stream)
        .map(CargoItem::getPieces)
        .reduce(0, Integer::sum);
  }

  private List<Flight> getDepartureFlights(IataCode iataCode, LocalDate date) {
    return flightRepository.findAllByDepartureAirportIATACode(iataCode).stream()
        .filter(flight -> isEqualDate.test(flight, date)).toList();
  }

  private List<Flight> getArrivalFlights(IataCode iataCode, LocalDate date) {
    return flightRepository.findAllByArrivalAirportIATACode(iataCode).stream()
        .filter(flight -> isEqualDate.test(flight, date)).toList();
  }

  private final BiPredicate<Flight, LocalDate> isEqualDate = (flight, localDate) -> localDate.isEqual(
      flight.getDepartureDate().withOffsetSameInstant(ZoneOffset.UTC).toLocalDate());
}
