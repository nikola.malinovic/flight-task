package org.task.flighttask.exception;

public class FlightNotFoundException extends RuntimeException {

  public FlightNotFoundException(final String reason) {
    super(reason);
  }
}
