package org.task.flighttask.exception;

import lombok.Data;

@Data
public class ApiError {

  private int status;

  private String title;

  private String detail;

  private String code;
}
