package org.task.flighttask.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

  // 400
  INVALID_REQUEST_PARAMETER(
      "Invalid Parameter",
      HttpStatus.BAD_REQUEST,
      "Invalid Request Parameters: %s",
      "ERR-" + HttpStatus.BAD_REQUEST.value()),
  // 404
  RESOURCE_NOT_FOUND_ERROR(
      "Resource not found", HttpStatus.NOT_FOUND, "%s", "ERR-" + HttpStatus.NOT_FOUND.value()),
  // 500
  INTERNAL_SERVER_ERROR(
      "Internal server error",
      HttpStatus.INTERNAL_SERVER_ERROR,
      "%s",
      "ERR-" + HttpStatus.INTERNAL_SERVER_ERROR.value());

  private final String title;

  private final HttpStatus status;

  private final String detail;

  private final String code;

  ErrorCode(String title, HttpStatus status, String detail, String code) {
    this.title = title;
    this.status = status;
    this.detail = detail;
    this.code = code;
  }

  public static ErrorCode fromCode(String text) {
    for (ErrorCode b : ErrorCode.values()) {
      if (b.code.equalsIgnoreCase(text)) {
        return b;
      }
    }
    return null;
  }

  public ApiError getApiError() {
    var error = new ApiError();
    error.setTitle(this.title);
    error.setStatus(status.value());
    error.setDetail(this.detail);
    error.setCode(this.code);
    return error;
  }

  public ApiError getApiError(Object... v) {
    var apiError = getApiError();
    apiError.setDetail(detail.formatted(v));
    return apiError;
  }

  public String getDescription(Object... v) {
    return detail.formatted(v);
  }

  public HttpStatus getStatus() {
    return this.status;
  }
}
