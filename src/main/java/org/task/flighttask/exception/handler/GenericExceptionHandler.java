package org.task.flighttask.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.task.flighttask.exception.ApiError;
import org.task.flighttask.exception.ErrorCode;

@Slf4j
@ControllerAdvice
public class GenericExceptionHandler {

  @ExceptionHandler
  public ResponseEntity<Object> handleGenericException(Exception ex) {
    log.error("Error occurred:", ex);
    ErrorCode errorCode = ErrorCode.INTERNAL_SERVER_ERROR;
    ApiError apiError = errorCode.getApiError();
    apiError.setDetail(ex.getMessage());
    return new ResponseEntity<>(apiError, HttpStatus.valueOf(apiError.getStatus()));
  }
}
