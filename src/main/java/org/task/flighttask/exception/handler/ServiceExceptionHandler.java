package org.task.flighttask.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.task.flighttask.exception.ApiError;
import org.task.flighttask.exception.ErrorCode;
import org.task.flighttask.exception.FlightNotFoundException;

@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ServiceExceptionHandler {

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<Object> handleMethodArgumentTypeMismatchException(
      MethodArgumentTypeMismatchException ex) {
    log.warn("Invalid Request parameter:", ex);
    ApiError apiError = ErrorCode.INVALID_REQUEST_PARAMETER.getApiError(ex.getMessage());
    apiError.setDetail(String.format("Invalid Request Parameter: %s", ex.getName()));
    return new ResponseEntity<>(apiError, HttpStatus.valueOf(apiError.getStatus()));
  }

  @ExceptionHandler(FlightNotFoundException.class)
  public ResponseEntity<Object> handleFlightNotFoundException(FlightNotFoundException ex) {
    log.warn("Could not find Flight:", ex);
    ApiError apiError = ErrorCode.RESOURCE_NOT_FOUND_ERROR.getApiError(ex.getMessage());
    return new ResponseEntity<>(apiError, HttpStatus.valueOf(apiError.getStatus()));
  }
}
