package org.task.flighttask.controller;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.task.flighttask.model.dto.BaggageStatisticsResponse;
import org.task.flighttask.model.dto.WeightStatisticsResponse;
import org.task.flighttask.model.entity.IataCode;
import org.task.flighttask.service.FlightService;

@RestController
@AllArgsConstructor
public class FlightController {

  private final FlightService flightService;

  @GetMapping("/weight-statistics")
  public ResponseEntity<WeightStatisticsResponse> getWeightStatistics(
      @RequestParam(name = "flight-number") int flightNumber,
      @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
    return ResponseEntity.ok(flightService.getWeightStatistics(flightNumber, date));
  }

  @GetMapping("/baggage-statistics")
  public ResponseEntity<BaggageStatisticsResponse> getBaggageStatistics(
      @RequestParam(name = "iata-code") IataCode iataCode,
      @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
    return ResponseEntity.ok(flightService.getBaggageStatistics(iataCode, date));
  }
}
