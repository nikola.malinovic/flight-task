package org.task.flighttask.controller;

import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.task.flighttask.model.dto.CargoDto;
import org.task.flighttask.model.dto.FlightDto;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.service.DataService;

@RestController
@AllArgsConstructor
public class DataController {

  private final DataService dataService;

  @PostMapping("/flights")
  public ResponseEntity<List<Flight>> addFlights(@RequestBody List<FlightDto> flights) {
    return ResponseEntity.ok(dataService.addFlights(flights));
  }

  @PostMapping("/cargo")
  public ResponseEntity<List<CargoDto>> addCargo(@RequestBody List<CargoDto> cargos) {
    return ResponseEntity.ok(dataService.addCargo(cargos));
  }
}
