package org.task.flighttask.util;

public class WeightUtil {

  public static double toKg(double weightLb) {
    if (weightLb < 0) {
      throw new RuntimeException("Weight can not be less than 0");
    }
    return weightLb / 2.205;
  }

  public static double toLb(double weightKg) {
    if (weightKg < 0) {
      throw new RuntimeException("Weight can not be less than 0");
    }
    return weightKg * 2.205;
  }
}
