package org.task.flighttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightTaskApplication {

  public static void main(String[] args) {
    SpringApplication.run(FlightTaskApplication.class, args);
  }
}
