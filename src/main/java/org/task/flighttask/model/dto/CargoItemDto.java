package org.task.flighttask.model.dto;

import org.task.flighttask.model.entity.WeightUnit;

public record CargoItemDto(
    int id,
    int weight,
    WeightUnit weightUnit,
    int pieces) {

}
