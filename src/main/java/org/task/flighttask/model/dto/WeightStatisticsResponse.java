package org.task.flighttask.model.dto;

public record WeightStatisticsResponse(
    double cargoWeightKg,
    double cargoWeightLb,
    double baggageWeightKg,
    double baggageWeightLb,
    double totalWeightKg,
    double totalWeightLb) {

}
