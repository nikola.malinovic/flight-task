package org.task.flighttask.model.dto;

import org.task.flighttask.model.entity.IataCode;

public record FlightDto(
    int flightId,
    int flightNumber,
    IataCode departureAirportIATACode,
    IataCode arrivalAirportIATACode,
    String departureDate) {

}
