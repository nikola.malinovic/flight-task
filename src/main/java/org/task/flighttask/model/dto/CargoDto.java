package org.task.flighttask.model.dto;

import java.util.List;

public record CargoDto(
    int flightId,
    List<CargoItemDto> baggage,
    List<CargoItemDto> cargo) {

}
