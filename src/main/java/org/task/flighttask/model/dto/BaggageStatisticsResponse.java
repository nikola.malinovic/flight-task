package org.task.flighttask.model.dto;

public record BaggageStatisticsResponse(
    int flightsDeparting,
    int flightsArriving,
    int baggagePiecesArriving,
    int baggagePiecesDeparting) {

}
