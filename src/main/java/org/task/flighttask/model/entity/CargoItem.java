package org.task.flighttask.model.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CargoItem {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  private int flightId;

  @PositiveOrZero
  private int weight;

  @Enumerated(EnumType.STRING)
  private WeightUnit weightUnit;

  @PositiveOrZero
  private int pieces;

  @Enumerated(EnumType.STRING)
  private CargoType cargoType;
}
