package org.task.flighttask.model.entity;

public enum CargoType {
  BAGGAGE,
  CARGO
}
