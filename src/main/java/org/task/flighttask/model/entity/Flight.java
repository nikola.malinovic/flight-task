package org.task.flighttask.model.entity;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

  @Id
  private int flightId;

  private int flightNumber;

  @Enumerated(EnumType.STRING)
  @Column(name = "departure_airport_iata_code")
  private IataCode departureAirportIATACode;

  @Enumerated(EnumType.STRING)
  @Column(name = "arrival_airport_iata_code")
  private IataCode arrivalAirportIATACode;

  private OffsetDateTime departureDate;
}
