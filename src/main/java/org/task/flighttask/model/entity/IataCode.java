package org.task.flighttask.model.entity;

public enum IataCode {
  SEA,
  YYZ,
  YYT,
  ANC,
  LAX,
  MIT,
  LEW,
  GDN,
  KRK,
  PPX
}
