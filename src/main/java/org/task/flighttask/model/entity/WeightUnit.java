package org.task.flighttask.model.entity;

public enum WeightUnit {
  kg,
  lb
}
