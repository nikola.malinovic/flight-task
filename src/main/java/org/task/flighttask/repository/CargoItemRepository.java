package org.task.flighttask.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.task.flighttask.model.entity.CargoItem;
import org.task.flighttask.model.entity.CargoType;

public interface CargoItemRepository extends JpaRepository<CargoItem, Integer> {

  List<CargoItem> findByFlightIdAndCargoType(int flightId, CargoType cargoType);
}
