package org.task.flighttask.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.model.entity.IataCode;

public interface FlightRepository extends JpaRepository<Flight, Integer> {

  List<Flight> findAllByFlightNumber(int flightNumber);

  List<Flight> findAllByDepartureAirportIATACode(IataCode iataCode);

  List<Flight> findAllByArrivalAirportIATACode(IataCode iataCode);
}
