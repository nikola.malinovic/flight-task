package org.task.flighttask;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = BaseIntegrationTest.DockerPostgreDataSourceInitializer.class)
@Testcontainers
@Sql(scripts = "/cleanup-data.sql")
public class BaseIntegrationTest {

  private static final String POSTGRES = "postgres";

  public static PostgreSQLContainer<?> postgresDBContainer;

  @BeforeAll
  static void setUp() {
    postgresDBContainer = new PostgreSQLContainer<>("postgres:13")
        .withNetworkAliases(POSTGRES)
        .withUsername(POSTGRES)
        .withPassword(POSTGRES)
        .withDatabaseName(POSTGRES)
        .withExposedPorts(5432);
    postgresDBContainer.start();
  }

  public static class DockerPostgreDataSourceInitializer implements
      ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

      TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext,
          "spring.jpa.show-sql=true",
          "spring.datasource.url=" + postgresDBContainer.getJdbcUrl(),
          "spring.datasource.username=" + postgresDBContainer.getUsername(),
          "spring.datasource.password=" + postgresDBContainer.getPassword()
      );
    }
  }
}
