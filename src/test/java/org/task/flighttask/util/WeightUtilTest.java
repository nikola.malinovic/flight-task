package org.task.flighttask.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.task.flighttask.util.WeightUtil.toKg;
import static org.task.flighttask.util.WeightUtil.toLb;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class WeightUtilTest {

  @ParameterizedTest
  @ValueSource(doubles = {0.0, 1.0, 5.0, 20.0})
  @DisplayName("Convert to KG positive numbers")
  void convertToKgPositiveNumbers(double input) {
    // when
    double result = toKg(input);

    // then
    assertEquals(input / 2.205, result);
  }

  @ParameterizedTest
  @ValueSource(doubles = {-1.0, -5.0, -20.0})
  @DisplayName("Convert to KG negative numbers")
  void convertToKgNegativeNumbers(double input) {
    // when
    var exception = assertThrows(RuntimeException.class, () -> toKg(input));

    // then
    assertEquals("Weight can not be less than 0", exception.getMessage());
  }

  @ParameterizedTest
  @ValueSource(doubles = {0.0, 1.0, 5.0, 20.0})
  @DisplayName("Convert to LB positive numbers")
  void convertToLbPositiveNumbers(double input) {
    // when
    double result = toLb(input);

    // then
    assertEquals(input * 2.205, result);
  }

  @ParameterizedTest
  @ValueSource(doubles = {-1.0, -5.0, -20.0})
  @DisplayName("Convert to LB negative numbers")
  void convertToLbNegativeNumbers(double input) {
    // when
    var exception = assertThrows(RuntimeException.class, () -> toLb(input));

    // then
    assertEquals("Weight can not be less than 0", exception.getMessage());
  }
}
