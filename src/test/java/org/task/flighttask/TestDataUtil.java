package org.task.flighttask;

import java.time.OffsetDateTime;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.model.entity.IataCode;

public class TestDataUtil {

  public static Flight getFlight() {
    return Flight.builder()
        .flightId(1)
        .flightNumber(11)
        .departureAirportIATACode(IataCode.ANC)
        .arrivalAirportIATACode(IataCode.GDN)
        .departureDate(OffsetDateTime.parse("2022-01-01T12:00:00+01:00"))
        .build();
  }
}
