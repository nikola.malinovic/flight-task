package org.task.flighttask.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.task.flighttask.TestDataUtil.getFlight;
import static org.task.flighttask.util.WeightUtil.toLb;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.task.flighttask.BaseIntegrationTest;
import org.task.flighttask.exception.ApiError;
import org.task.flighttask.model.dto.BaggageStatisticsResponse;
import org.task.flighttask.model.dto.WeightStatisticsResponse;
import org.task.flighttask.model.entity.CargoItem;
import org.task.flighttask.model.entity.CargoType;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.model.entity.IataCode;
import org.task.flighttask.model.entity.WeightUnit;
import org.task.flighttask.repository.CargoItemRepository;
import org.task.flighttask.repository.FlightRepository;

public class FlightControllerIntegrationTest extends BaseIntegrationTest {

  private static WireMockServer wireMockServer;

  @Autowired
  private FlightRepository flightRepository;

  @Autowired
  private CargoItemRepository cargoItemRepository;

  @Autowired
  private TestRestTemplate testRestTemplate;

  @BeforeAll
  static void startWireMock() {
    wireMockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().port(9991));
    wireMockServer.start();
  }

  @AfterAll
  static void stopWireMock() {
    wireMockServer.stop();
  }

  @Test
  void testWireMock() {
    assertTrue(wireMockServer.isRunning());
  }

  @Nested
  @DisplayName("Get Weight Statistics Test")
  class GetWeightStatistics {

    @Test
    @DisplayName("When Flight and Cargo Items exist, return correct values")
    void flightAndCargoItemsExist() {
      // given
      Flight flight = flightRepository.save(getFlight());
      LocalDate date = flight.getDepartureDate().toLocalDate();

      CargoItem cargo = CargoItem.builder()
          .id(1)
          .flightId(flight.getFlightId())
          .weight(11)
          .weightUnit(WeightUnit.kg)
          .cargoType(CargoType.CARGO)
          .build();
      CargoItem baggage = CargoItem.builder()
          .id(2)
          .flightId(flight.getFlightId())
          .weight(22)
          .weightUnit(WeightUnit.kg)
          .cargoType(CargoType.BAGGAGE)
          .build();
      cargoItemRepository.saveAll(List.of(cargo, baggage));

      // when
      var url = String.format("/weight-statistics?flight-number=%d&date=%s",
          flight.getFlightNumber(), date);
      var response = testRestTemplate.getForEntity(url, WeightStatisticsResponse.class);

      // then
      assertEquals(HttpStatus.OK, response.getStatusCode());
      assertEquals(200, response.getStatusCodeValue());
      assertNotNull(response.getBody());
      assertEquals(11, response.getBody().cargoWeightKg());
      assertEquals(toLb(11), response.getBody().cargoWeightLb());
      assertEquals(22, response.getBody().baggageWeightKg());
      assertEquals(toLb(22), response.getBody().baggageWeightLb());
      assertEquals(33, response.getBody().totalWeightKg());
      assertEquals(toLb(33), response.getBody().totalWeightLb());
    }

    @Test
    @DisplayName("When Cargo Items do not exist, return zero values")
    void cargoItemsDoNotExist() {
      // given
      Flight flight = flightRepository.save(getFlight());
      LocalDate date = flight.getDepartureDate().toLocalDate();

      // when
      var url = String.format("/weight-statistics?flight-number=%d&date=%s",
          flight.getFlightNumber(), date);
      var response = testRestTemplate.getForEntity(url, WeightStatisticsResponse.class);

      // then
      assertEquals(HttpStatus.OK, response.getStatusCode());
      assertEquals(200, response.getStatusCodeValue());
      assertNotNull(response.getBody());
      assertEquals(0, response.getBody().cargoWeightKg());
      assertEquals(0, response.getBody().cargoWeightLb());
      assertEquals(0, response.getBody().baggageWeightKg());
      assertEquals(0, response.getBody().baggageWeightLb());
      assertEquals(0, response.getBody().totalWeightKg());
      assertEquals(0, response.getBody().totalWeightLb());
    }

    @Test
    @DisplayName("When Flight for provided date does not exist, then throw Flight Not Found exception")
    void dateDoesNotExist() {
      // given
      Flight flight = flightRepository.save(getFlight());

      // when
      var url = String.format("/weight-statistics?flight-number=%d&date=2022-02-02",
          flight.getFlightNumber());
      var response = testRestTemplate.getForEntity(url, ApiError.class);

      // then
      assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
      assertEquals(404, response.getStatusCodeValue());
      assertEquals("Resource not found", response.getBody().getTitle());
      assertEquals("Flight does not exist", response.getBody().getDetail());
      assertEquals("ERR-404", response.getBody().getCode());
    }

    @Test
    @DisplayName("When Flight for provided flight number does not exist, then throw Flight Not Found exception")
    void flightNumberDoesNotExist() {
      // when
      var url = "/weight-statistics?flight-number=1&date=2022-01-01";
      var response = testRestTemplate.getForEntity(url, ApiError.class);

      // then
      assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
      assertEquals(404, response.getStatusCodeValue());
      assertEquals("Resource not found", response.getBody().getTitle());
      assertEquals("Flight does not exist", response.getBody().getDetail());
      assertEquals("ERR-404", response.getBody().getCode());
    }
  }

  @Nested
  @DisplayName("Get Baggage Statistics Test")
  class GetBaggageStatistics {

    @Test
    @DisplayName("When Flights do exist, return correct values")
    void flightsExist() {
      // given
      IataCode iataCode = IataCode.YYZ;

      Flight departingFlight = getFlight().toBuilder()
          .flightId(1)
          .departureAirportIATACode(iataCode)
          .build();
      Flight arrivingFlight = getFlight().toBuilder()
          .flightId(2)
          .arrivalAirportIATACode(iataCode)
          .build();
      flightRepository.saveAll(List.of(departingFlight, arrivingFlight));

      CargoItem baggageArriving = CargoItem.builder()
          .id(1)
          .flightId(arrivingFlight.getFlightId())
          .cargoType(CargoType.BAGGAGE)
          .pieces(11)
          .build();
      CargoItem baggageDeparting = CargoItem.builder()
          .id(2)
          .flightId(departingFlight.getFlightId())
          .cargoType(CargoType.BAGGAGE)
          .pieces(22)
          .build();
      cargoItemRepository.saveAll(List.of(baggageArriving, baggageDeparting));

      LocalDate date = departingFlight.getDepartureDate().toLocalDate();

      // when
      var url = String.format("/baggage-statistics?iata-code=%s&date=%s", iataCode, date);
      var response = testRestTemplate.getForEntity(url, BaggageStatisticsResponse.class);

      // then
      assertEquals(HttpStatus.OK, response.getStatusCode());
      assertEquals(200, response.getStatusCodeValue());
      assertNotNull(response.getBody());
      assertEquals(1, response.getBody().flightsDeparting());
      assertEquals(1, response.getBody().flightsArriving());
      assertEquals(11, response.getBody().baggagePiecesArriving());
      assertEquals(22, response.getBody().baggagePiecesDeparting());
    }

    @Test
    @DisplayName("When Flights do not exist, return zero values")
    void flightsDoNotExist() {
      // when
      var url = "/baggage-statistics?iata-code=ANC&date=2022-01-01";
      var response = testRestTemplate.getForEntity(url, BaggageStatisticsResponse.class);

      // then
      assertEquals(HttpStatus.OK, response.getStatusCode());
      assertEquals(200, response.getStatusCodeValue());
      assertNotNull(response.getBody());
      assertEquals(0, response.getBody().flightsDeparting());
      assertEquals(0, response.getBody().flightsArriving());
      assertEquals(0, response.getBody().baggagePiecesArriving());
      assertEquals(0, response.getBody().baggagePiecesDeparting());
    }
  }
}
