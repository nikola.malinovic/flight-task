package org.task.flighttask.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.task.flighttask.model.dto.BaggageStatisticsResponse;
import org.task.flighttask.model.dto.WeightStatisticsResponse;
import org.task.flighttask.model.entity.IataCode;
import org.task.flighttask.service.FlightService;

@ExtendWith(MockitoExtension.class)
class FlightControllerTest {

  @Mock
  FlightService flightService;

  @InjectMocks
  FlightController underTest;

  @Test
  @DisplayName("When getWeightStatistics is called, then return response from service")
  void getWeightStatistics() {
    // given
    int flightNumber = 1;
    LocalDate date = LocalDate.now();
    var weightStatisticsResponse = new WeightStatisticsResponse(1, 2, 3, 4, 5, 6);

    when(flightService.getWeightStatistics(flightNumber, date)).thenReturn(
        weightStatisticsResponse);

    // when
    var result = underTest.getWeightStatistics(flightNumber, date);

    // then
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(200, result.getStatusCodeValue());
    assertEquals(weightStatisticsResponse, result.getBody());
  }

  @Test
  @DisplayName("When getBaggageStatistics is called, then return response from service")
  void getBaggageStatistics() {
    // given
    IataCode iataCode = IataCode.ANC;
    LocalDate date = LocalDate.now();
    var baggageStatisticsResponse = new BaggageStatisticsResponse(1, 2, 3, 4);

    when(flightService.getBaggageStatistics(iataCode, date)).thenReturn(baggageStatisticsResponse);

    // when
    var result = underTest.getBaggageStatistics(iataCode, date);

    // then
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(200, result.getStatusCodeValue());
    assertEquals(baggageStatisticsResponse, result.getBody());
  }
}
