package org.task.flighttask.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.task.flighttask.TestDataUtil.getFlight;
import static org.task.flighttask.util.WeightUtil.toLb;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.task.flighttask.exception.FlightNotFoundException;
import org.task.flighttask.model.entity.CargoItem;
import org.task.flighttask.model.entity.CargoType;
import org.task.flighttask.model.entity.Flight;
import org.task.flighttask.model.entity.IataCode;
import org.task.flighttask.model.entity.WeightUnit;
import org.task.flighttask.repository.CargoItemRepository;
import org.task.flighttask.repository.FlightRepository;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

  @Mock
  FlightRepository flightRepository;

  @Mock
  CargoItemRepository cargoItemRepository;

  @InjectMocks
  FlightService underTest;

  @Nested
  @DisplayName("Get Weight Statistics test")
  class GetWeightStatistics {

    @Test
    @DisplayName("When Flight and Cargo Items exist, then return correct values")
    void successful() {
      // given
      Flight flight = getFlight();
      LocalDate date = flight.getDepartureDate().toLocalDate();

      CargoItem cargOne = CargoItem.builder()
          .id(1)
          .flightId(flight.getFlightId())
          .weight(11)
          .weightUnit(WeightUnit.kg)
          .pieces(111)
          .cargoType(CargoType.CARGO)
          .build();
      CargoItem cargoTwo = CargoItem.builder()
          .id(2)
          .flightId(flight.getFlightId())
          .weight(22)
          .weightUnit(WeightUnit.kg)
          .pieces(222)
          .cargoType(CargoType.CARGO)
          .build();

      CargoItem baggageOne = CargoItem.builder()
          .id(3)
          .flightId(flight.getFlightId())
          .weight(33)
          .weightUnit(WeightUnit.kg)
          .pieces(333)
          .cargoType(CargoType.BAGGAGE)
          .build();
      CargoItem baggageTwo = CargoItem.builder()
          .id(4)
          .flightId(flight.getFlightId())
          .weight(44)
          .weightUnit(WeightUnit.kg)
          .pieces(444)
          .cargoType(CargoType.BAGGAGE)
          .build();

      when(flightRepository.findAllByFlightNumber(flight.getFlightNumber()))
          .thenReturn(List.of(flight));
      when(cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.CARGO))
          .thenReturn(List.of(cargOne, cargoTwo));
      when(cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.BAGGAGE))
          .thenReturn(List.of(baggageOne, baggageTwo));

      // when
      var result = underTest.getWeightStatistics(flight.getFlightNumber(), date);

      // then
      assertEquals(33, result.cargoWeightKg());
      assertEquals(toLb(33), result.cargoWeightLb());
      assertEquals(77, result.baggageWeightKg());
      assertEquals(toLb(77), result.baggageWeightLb());
      assertEquals(110, result.totalWeightKg());
      assertEquals(toLb(110), result.totalWeightLb());
    }

    @Test
    @DisplayName("When Flight does not exist, throw Flight Not Found Exception")
    void flightDoesNotExist() {
      // when
      FlightNotFoundException exception = assertThrows(
          FlightNotFoundException.class, () -> underTest.getWeightStatistics(1, LocalDate.now()));

      // then
      assertEquals("Flight does not exist", exception.getMessage());
    }

    @Test
    @DisplayName("When Baggage and Cargo do not exist, return zero values")
    void cargoItemsDoNotExist() {
      // given
      Flight flight = getFlight();
      LocalDate date = flight.getDepartureDate().toLocalDate();

      when(flightRepository.findAllByFlightNumber(flight.getFlightNumber()))
          .thenReturn(List.of(flight));
      when(cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.CARGO))
          .thenReturn(List.of());
      when(cargoItemRepository.findByFlightIdAndCargoType(flight.getFlightId(), CargoType.BAGGAGE))
          .thenReturn(List.of());

      // when
      var result = underTest.getWeightStatistics(flight.getFlightNumber(), date);

      // then
      assertEquals(0, result.cargoWeightKg());
      assertEquals(0, result.cargoWeightLb());
      assertEquals(0, result.baggageWeightKg());
      assertEquals(0, result.baggageWeightLb());
      assertEquals(0, result.totalWeightKg());
      assertEquals(0, result.totalWeightLb());
    }
  }

  @Nested
  @DisplayName("Get Baggage Statistics test")
  class GetBaggageStatistics {

    @Test
    @DisplayName("When flights exist, then return correct values")
    void flightsExist() {
      // given
      IataCode iataCode = IataCode.ANC;
      LocalDate date = LocalDate.parse("2022-01-01");
      OffsetDateTime offsetDateTime = OffsetDateTime.parse("2022-01-01T12:00:00+01:00");

      Flight departureFlightOne = Flight.builder()
          .flightId(1)
          .flightNumber(11)
          .departureAirportIATACode(IataCode.ANC)
          .departureDate(offsetDateTime)
          .build();
      Flight departureFlightTwo = Flight.builder()
          .flightId(2)
          .departureAirportIATACode(IataCode.ANC)
          .departureDate(offsetDateTime)
          .build();
      Flight arrivalFlightOne = Flight.builder()
          .flightId(3)
          .arrivalAirportIATACode(IataCode.ANC)
          .departureDate(offsetDateTime)
          .build();
      Flight arrivalFlightTwo = Flight.builder()
          .flightId(4)
          .arrivalAirportIATACode(IataCode.ANC)
          .departureDate(offsetDateTime)
          .build();

      CargoItem departingBaggageOne = CargoItem.builder().id(1).pieces(11).build();
      CargoItem departingBaggageTwo = CargoItem.builder().id(2).pieces(22).build();
      CargoItem arrivingBaggageOne = CargoItem.builder().id(3).pieces(33).build();
      CargoItem arrivingBaggageTwo = CargoItem.builder().id(4).pieces(44).build();

      when(flightRepository.findAllByDepartureAirportIATACode(iataCode))
          .thenReturn(List.of(departureFlightOne, departureFlightTwo));
      when(flightRepository.findAllByArrivalAirportIATACode(iataCode))
          .thenReturn(List.of(arrivalFlightOne, arrivalFlightTwo));

      when(cargoItemRepository.findByFlightIdAndCargoType(departureFlightOne.getFlightId(),
          CargoType.BAGGAGE)).thenReturn(List.of(departingBaggageOne));
      when(cargoItemRepository.findByFlightIdAndCargoType(departureFlightTwo.getFlightId(),
          CargoType.BAGGAGE)).thenReturn(List.of(departingBaggageTwo));
      when(cargoItemRepository.findByFlightIdAndCargoType(arrivalFlightOne.getFlightId(),
          CargoType.BAGGAGE)).thenReturn(List.of(arrivingBaggageOne));
      when(cargoItemRepository.findByFlightIdAndCargoType(arrivalFlightTwo.getFlightId(),
          CargoType.BAGGAGE)).thenReturn(List.of(arrivingBaggageTwo));

      // when
      var result = underTest.getBaggageStatistics(iataCode, date);

      // then
      assertEquals(2, result.flightsDeparting());
      assertEquals(2, result.flightsArriving());
      assertEquals(77, result.baggagePiecesArriving());
      assertEquals(33, result.baggagePiecesDeparting());
    }

    @Test
    @DisplayName("When flights do not exist, then return zero values")
    void flightsDoNotExist() {
      // when
      var result = underTest.getBaggageStatistics(IataCode.ANC, LocalDate.now());

      // then
      assertEquals(0, result.flightsDeparting());
      assertEquals(0, result.flightsArriving());
      assertEquals(0, result.baggagePiecesDeparting());
      assertEquals(0, result.baggagePiecesArriving());
    }
  }
}
